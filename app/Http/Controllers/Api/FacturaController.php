<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Greenter\Ws\Services\SunatEndpoints;
use Greenter\See;
use Greenter\Report\Resolver\DefaultTemplateResolver;
use Greenter\Report\PdfReport;
use Greenter\Model\Client\Client;
use Greenter\Report\HtmlReport;
use Greenter\Model\Company\Company;
use Greenter\Model\Company\Address;
use Greenter\Model\Sale\Invoice;
use Greenter\Model\Sale\SaleDetail;
use Greenter\Model\Sale\Legend;

use DateTime;

class FacturaController extends Controller
{
    /**
    * @OA\Info(title="API Facturacion Electrónica", version="1.0")
    *
    * @OA\Server(url="http://localhost:8000")
    */

    /**
     * @OA\Post(
     * path="/api/generar-factura",
     * summary="Generar factura",
     * description="Generar Facturacion Electrónica",
     * operationId="authLogin",
     * tags={"Generar Factura"},

    *     @OA\RequestBody(
    *        required = true,
    *        description = "Venta",
    *        @OA\JsonContent(
    *             type="object",



    *             @OA\Property(
    *                property="Cliente",
    *                type="array",
    *                example={{
    *                  "TipoDoc": "6",
    *                  "NumDoc": "20000000001",
    *                  "RznSocial": "SolucionesMw",
    *                }},
    *                @OA\Items(
    *                      @OA\Property(
    *                         property="TipoDoc",
    *                         type="integer",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="NumDoc",
    *                         type="integer",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="SolucionesMw",
    *                         type="string",
    *                         example=""
    *                      ),
    *                ),
    *             ),







    *             @OA\Property(
    *                property="Emisor",
    *                type="array",
    *                example={{
    *                  "Ubigueo": "150101",
    *                  "Departamento": "LIMA",
    *                  "Provincia": "LIMA",
    *                  "Distrito": "LIMA",
    *                  "Urbanizacion": "-",
    *                  "Direccion": "Av. Villa Nueva 2213",
    *                  "CodLocal": "0000",
    *                }},
    *                @OA\Items(
    *                      @OA\Property(
    *                         property="Ubigueo",
    *                         type="integer",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Departamento",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Provincia",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Distrito",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Urbanizacion",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Direccion",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="CodLocal",
    *                         type="integer",
    *                         example=""
    *                      ),
    *                ),
    *             ),



    *             @OA\Property(
    *                property="Compania",
    *                type="array",
    *                example={{
    *                  "RUC": "20123456789",
    *                  "RznSocial": "GREEN SAC",
    *                  "NombreComercial": "GREEN",
    *                }},
    *                @OA\Items(
    *                      @OA\Property(
    *                         property="RUC",
    *                         type="integer",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="RznSocial",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="NombreComercial",
    *                         type="string",
    *                         example=""
    *                      ),
    *                ),
    *             ),

    
    *             @OA\Property(
    *                property="Venta",
    *                type="array",
    *                example={{
    *                  "TipoOperacion": "0101",
    *                  "TipoDoc": "01",
    *                  "Serie": "F001",
    *                  "Correlativo": "1",
    *                  "FechaEmision": "2020-08-24 13:05:00",
    *                  "TipoMoneda": "PEN",
    *                  "MtoOperGravadas": "340.00",
    *                  "MtoIGV": "61.20",
    *                  "TotalImpuestos": "61.20",
    *                  "ValorVenta": "340.00",
    *                  "SubTotal": "401.20",
    *                  "MtoImpVenta": "401.20",
    *                }},
    *                @OA\Items(
    *                      @OA\Property(
    *                         property="TipoOperacion",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="TipoDoc",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Serie",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Correlativo",
    *                         type="integer",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="FechaEmision",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="TipoMoneda",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="MtoOperGravadas",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="MtoIGV",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="TotalImpuestos",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="ValorVenta",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="SubTotal",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="MtoImpVenta",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                ),
    *             ),


    *             @OA\Property(
    *                property="Detalle",
    *                type="array",
    *                example={{
    *                  "CodProducto": "P001",
    *                  "Unidad": "NIU",
    *                  "Cantidad": "2",
    *                  "MtoValorUnitario": "50.00",
    *                  "Descripcion": "PRODUCTO 1",
    *                  "MtoBaseIgv": "100",
    *                  "PorcentajeIgv": "18.00",
    *                  "Igv": "18.00",
    *                  "TipAfeIgv": "10",
    *                  "TotalImpuestos": "18.00",
    *                  "MtoValorVenta": "100.00",
    *                  "MtoPrecioUnitario": "59.00"
    *                }, {
    *                  "CodProducto": "P002",
    *                  "Unidad": "NIU",
    *                  "Cantidad": "3",
    *                  "MtoValorUnitario": "80.00",
    *                  "Descripcion": "PRODUCTO 2",
    *                  "MtoBaseIgv": "240",
    *                  "PorcentajeIgv": "18.00",
    *                  "Igv": "43.20",
    *                  "TipAfeIgv": "10",
    *                  "TotalImpuestos": "43.20",
    *                  "MtoValorVenta": "240.00",
    *                  "MtoPrecioUnitario": "94.40"
    *                }},
    *                @OA\Items(
    *                      @OA\Property(
    *                         property="CodProducto",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Unidad",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Cantidad",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="MtoValorUnitario",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Descripcion",
    *                         type="string",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="MtoBaseIgv",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="PorcentajeIgv",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="Igv",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="TipAfeIgv",
    *                         type="integer",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="TotalImpuestos",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="MtoValorVenta",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                      @OA\Property(
    *                         property="MtoPrecioUnitario",
    *                         type="number",
    *                         format="double",
    *                         example=""
    *                      ),
    *                ),
    *             ),





    *        ),
    *     ),



     * @OA\Response(
     *    response=422,
     *    description="Respuesta incorrecta",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sin resultados")
     *        )
     *     )
     * )
    */
    public function index(Request $request)
    {
        $see = config('config.see');
        $data =  $request->all();
        $datosCliente =  $data['Cliente'];
       
        $client = (new Client())
        ->setTipoDoc($data['Cliente'][0]['TipoDoc']) //  0 - No domiciliado | 1 - DNI | 6 - RUC
        ->setNumDoc($data['Cliente'][0]['NumDoc'])
        ->setRznSocial($data['Cliente'][0]['RznSocial']);


        $address = (new Address())
        ->setUbigueo($data['Emisor'][0]['Ubigueo'])
        ->setDepartamento($data['Emisor'][0]['Departamento'])
        ->setProvincia($data['Emisor'][0]['Provincia'])
        ->setDistrito($data['Emisor'][0]['Distrito'])
        ->setUrbanizacion($data['Emisor'][0]['Urbanizacion'])
        ->setDireccion($data['Emisor'][0]['Direccion'])
        ->setCodLocal('0000'); // Codigo de establecimiento asignado por SUNAT, 0000 por defecto.

        $company = (new Company())
        ->setRuc($data['Compania'][0]['RUC'])
        ->setRazonSocial('RznSocial')
        ->setNombreComercial('NombreComercial')
        ->setAddress($address);

        // Venta
        $invoice = (new Invoice())
        ->setUblVersion('2.1')
        ->setTipoOperacion($data['Venta'][0]['TipoOperacion']) // Venta - Catalog. 51
        ->setTipoDoc($data['Venta'][0]['TipoDoc']) // Factura - Catalog. 01 
        ->setSerie($data['Venta'][0]['Serie'])
        ->setCorrelativo($data['Venta'][0]['Correlativo'])
        ->setFechaEmision(new DateTime($data['Venta'][0]['FechaEmision']))
        ->setTipoMoneda($data['Venta'][0]['TipoMoneda']) // Sol - Catalog. 02
        ->setCompany($company)
        ->setClient($client)
        ->setMtoOperGravadas($data['Venta'][0]['MtoOperGravadas'])
        ->setMtoIGV($data['Venta'][0]['MtoIGV'])
        ->setTotalImpuestos($data['Venta'][0]['TotalImpuestos'])
        ->setValorVenta($data['Venta'][0]['ValorVenta'])
        ->setSubTotal($data['Venta'][0]['SubTotal'])
        ->setMtoImpVenta($data['Venta'][0]['MtoImpVenta'])
        ;

        $contador = 0;
        $arreglo_detalle = [];
        foreach($data['Detalle'] as $item){
                $contador++;

                ${"loop" . $contador}= (new SaleDetail())
                ->setCodProducto($item['CodProducto'])
                ->setUnidad($item['Unidad']) // Unidad - Catalog. 03
                ->setCantidad($item['Cantidad'])
                ->setMtoValorUnitario($item['MtoValorUnitario'])
                ->setDescripcion($item['Descripcion'])
                ->setMtoBaseIgv($item['MtoBaseIgv'])
                ->setPorcentajeIgv($item['PorcentajeIgv']) // 18%
                ->setIgv($item['Igv'])
                ->setTipAfeIgv($item['TipAfeIgv']) // Gravado Op. Onerosa - Catalog. 07
                ->setTotalImpuestos($item['TotalImpuestos']) // Suma de impuestos en el detalle
                ->setMtoValorVenta($item['MtoValorVenta'])
                ->setMtoPrecioUnitario($item['MtoPrecioUnitario']);
                array_push($arreglo_detalle,${"loop" . $contador});
        }


        $legend = (new Legend())
        ->setCode('1000') // Monto en letras - Catalog. 52
        ->setValue('SON CUATROCIENTOS UN 20/100 SOLES');

        $invoice->setDetails($arreglo_detalle)
        ->setLegends([$legend]);


        //Envío a SUNAT        
        $result = $see->send($invoice);

        // Guardar XML firmado digitalmente.
        file_put_contents($invoice->getName().'.xml',
                        $see->getFactory()->getLastXml());

        // Verificamos que la conexión con SUNAT fue exitosa.
        if (!$result->isSuccess()) {
            // Mostrar error al conectarse a SUNAT.
            echo 'Codigo Error: '.$result->getError()->getCode();
            echo 'Mensaje Error: '.$result->getError()->getMessage();
            exit();
        }
        // Guardamos el CDR
        file_put_contents('R-'.$invoice->getName().'.zip', $result->getCdrZip());


        //LECTURA DEL CDR
        
        $cdr = $result->getCdrResponse();

        $code = (int)$cdr->getCode();

        if ($code === 0) {
            echo 'ESTADO: ACEPTADA'.PHP_EOL;
            if (count($cdr->getNotes()) > 0) {
                echo 'OBSERVACIONES:'.PHP_EOL;
                // Corregir estas observaciones en siguientes emisiones.
                var_dump($cdr->getNotes());
            }  
        } else if ($code >= 2000 && $code <= 3999) {
            echo 'ESTADO: RECHAZADA'.PHP_EOL;
        } else {
            /* Esto no debería darse, pero si ocurre, es un CDR inválido que debería tratarse como un error-excepción. */
            /*code: 0100 a 1999 */
            echo 'Excepción';
        }
        

        echo $cdr->getDescription().PHP_EOL;  
        
        //FIN LECTURA CDR


        // file_put_contents('invoice.pdf', $pdf);

    }
}
