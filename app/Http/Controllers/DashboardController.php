<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //ecommerce
    public function dashboardEcommerce(){
        return view('pages.dashboard-ecommerce');
    }
    // analystic
    public function dashboardAnalytics(){

       
            $pageConfigs = [
                'theme' => 'light',
                'navbarColor' => 'bg-primary',
                'navbarType' => 'static',
                'footerType' => 'sticky',
                'bodyClass' => 'testClass',
                'mainLayoutType'=>'horizontal-menu'
            ];


        return view('pages.dashboard-analytics',[
            'pageConfigs' => $pageConfigs

        ]);
    }
}
